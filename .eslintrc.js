module.exports = {
  root: true,

  env: {
    node: true,
  },

  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],

  rules: {
    'no-param-reassign': 'off',
    camelcase: 'off',
    'no-console': 'off',
    'no-debugger': 'off',
    'consistent-return': 0,
    'linebreak-style': 0
  },

  parserOptions: {
    parser: 'babel-eslint',
  },

  'extends': [
    'plugin:vue/essential',
    '@vue/airbnb'
  ]
};
