import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home_page',
      component: () =>
        import(/* webpackChunkName: "HomePage" */ './pages/HomePage.vue'),
    },
    {
      path: '/detail',
      name: 'detail_page',
      component: () =>
        import(/* webpackChunkName: "DetailPage" */ './pages/DetailPage.vue'),
    },
  ],
});

export default router;
