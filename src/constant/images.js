const LOGO_CANON = require('../image/canon.png');
const LOGO_AUORA = require('../image/Aurora.png');
const LOGO_AVISION = require('../image/Avision.png');
const LOGO_SONY = require('../image/Sony-logo.jpg');
const LOGO_TOSHIBA = require('../image/toshiba.png');
const LOGO_PANASONIC = require('../image/panasonic.png');
const MACHINE_PHOTOCOPY = require('../image/may-photocopy-canon-ir2006n-ir2206n.jpg')

const arrImgMachine = [
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/may-photocopy-canon-ir2006n-ir2206n.jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/may-photocopy-canon-ir2006n-ir2206n.jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/may-photocopy-canon-ir2006n-ir2206n.jpg'),
    coast: '4799999đ',
  }];
const arrImgIN = [
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/May in Fuji Xerox DocuPrint P225 db.jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/May in Fuji Xerox DocuPrint P225 db.jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/May in Fuji Xerox DocuPrint P225 db.jpg'),
    coast: '4799999đ',
  }];
const arrImgFax = [
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/Fax Brother 2840(1).jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/Fax Brother 2840(1).jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/Fax Brother 2840(1).jpg'),
    coast: '4799999đ',
  }];
const arrImgScan = [
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/ScanMate i940.jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/ScanMate i940.jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/ScanMate i940.jpg'),
    coast: '4799999đ',
  }];
const arrImgDestroyPage = [
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/May huy IDEAL 8240.jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/May huy IDEAL 8240.jpg'),
    coast: '4799999đ',
  },
  {
    name: 'Máy photocopy Fụi xerox',
    img: require('../image/May huy IDEAL 8240.jpg'),
    coast: '4799999đ',
  }];
const arrLogo = [
  LOGO_CANON,
  LOGO_AUORA,
  LOGO_AVISION,
  LOGO_SONY,
  LOGO_TOSHIBA,
  LOGO_PANASONIC,
];
export default {
  arrLogo,
  arrImgMachine,
  MACHINE_PHOTOCOPY,
  arrImgIN,
  arrImgFax,
  arrImgDestroyPage,
  arrImgScan,
};
