export default {
  updateAppVersion(state, version) {
    state.version = version;
  },
};
