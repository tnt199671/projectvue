import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import colors from 'vuetify/es5/util/colors';
import '@fortawesome/fontawesome-free/css/all.css';
import ProductZoomer from 'vue-product-zoomer';

Vue.use(Vuetify, {
  theme: {
    primary: '#121212',
    accent: colors.grey.darken3,
    secondary: colors.amber.darken3,
    info: colors.teal.lighten1,
    warning: colors.amber.base,
    error: colors.deepOrange.accent4,
    success: colors.green.accent3,
  },
  iconfont: 'fa',
  icons: {
    cancel: 'fas fa-ban',
    menu: 'fas fa-ellipsis-v',
  },
});

Vue.use(ProductZoomer);
